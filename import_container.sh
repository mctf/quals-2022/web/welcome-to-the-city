#!/bin/bash

docker image rm ctf3:latest
docker import ctf3_cont
ctf3_image_name=$(docker images | awk '{print $3}' | awk 'NR==2')
docker image tag $ctf3_image_name ctf3:latest
