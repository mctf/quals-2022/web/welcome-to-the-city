FROM ctf3:latest
ARG CTF3_FLAG
CMD ["/bin/bash"]
ADD config_flag.py /root/
RUN python3 /root/config_flag.py $CTF3_FLAG;rm /root/config_flag.py
WORKDIR /home/server
